/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as ts from 'typescript';

/**
 * @param sourceFile AST of the processed file
 * @param nodeStartPos node's start position (`node.getStart()`)
 * @param nodeEndPos node's end position (`node.getEnd()`)
 * @param nodeStartLine node's start line. NB! line count from 1, in contrast to TSC
 * @returns column index of the node's end if node is located on one line, line's end otherwise
 */
export function getNodeOrLineEnd(
  sourceFile: ts.SourceFile,
  nodeStartPos: number,
  nodeEndPos: number,
  nodeStartLine: number
): number {
  const pos = sourceFile.getLineAndCharacterOfPosition(nodeEndPos);
  // TSC counts lines and columns from zero
  return pos.line + 1 === nodeStartLine
    ? pos.character + 1
    : sourceFile.getLineEndOfPosition(nodeStartPos) + 1;
}
