#  IIFEs as namespace declarations are not supported

Rule ``arkts-no-iife``

**Severity: error**

ArkTS does not support IIFEs as namespace declarations because anonymous
functions in the language cannot serve as namespaces.
Use regular syntax for namespaces instead.


## TypeScript


```

    var C = (function() {
        function C(n: number) {
            this.p = n // Compile-time error only with noImplicitThis
        }
        C.staticProperty = 0
        return C
    })()
    C.staticProperty = 1

```

## ArkTS


```

    namespace C {
        // ...
    }

```


