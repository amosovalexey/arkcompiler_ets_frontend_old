#  ``for-of`` is supported only for arrays and strings

Rule ``arkts-for-of-str-arr``

**Severity: error**

ArkTS supports the iteration over arrays and strings by the ``for .. of`` loop,
but does not support the iteration of objects content. All typed arrays from
the standard library (for example, ``Int32Array``) are also supported.


## TypeScript


```

    let a: Set<number> = new Set([1, 2, 3])
    for (let s of a) {
        console.log(s)
    }

```

## ArkTS


```

    let a: Set<number> = new Set([1, 2, 3])
    let numbers = Array.from(a.values())
    for (let n of numbers) {
        console.log(n)
    }

```

## See also

- Recipe 080:  ``for .. in`` is not supported (``arkts-no-for-in``)


