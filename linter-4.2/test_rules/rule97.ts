class Point {
    x: number = 1
    y: number = 2
}

type PointKeys = keyof Point  // The type of PointKeys is "x" | "y"

function getPropertyValue(obj: Point, key: PointKeys) {
    return obj[key]
}

let obj = new Point()
console.log(getPropertyValue(obj, "x"))
console.log(getPropertyValue(obj, "y"))

function getPropertyValue1(obj: Point, key: string): number {
    if (key == "x") {
        return obj.x
    }
    if (key == "y") {
        return obj.y
    }
    throw new Error()
}

function main(): void {
    let obj = new Point()
    console.log(getPropertyValue1(obj, "x"))
    console.log(getPropertyValue1(obj, "y"))
}