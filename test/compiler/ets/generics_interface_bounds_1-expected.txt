{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 8
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 5
                    },
                    "end": {
                      "line": 2,
                      "column": 8
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a0",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "T",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 13
                              },
                              "end": {
                                "line": 2,
                                "column": 14
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 13
                            },
                            "end": {
                              "line": 2,
                              "column": 15
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 13
                          },
                          "end": {
                            "line": 2,
                            "column": 15
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 9
                        },
                        "end": {
                          "line": 2,
                          "column": 15
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 9
                      },
                      "end": {
                        "line": 2,
                        "column": 15
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 17
                    },
                    "end": {
                      "line": 2,
                      "column": 21
                    }
                  }
                },
                "declare": true,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 8
                  },
                  "end": {
                    "line": 2,
                    "column": 21
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 21
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 21
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 15
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "A",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "typeParameters": {
        "type": "TSTypeParameterDeclaration",
        "params": [
          {
            "type": "TSTypeParameter",
            "name": {
              "type": "Identifier",
              "name": "T",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 13
                },
                "end": {
                  "line": 1,
                  "column": 14
                }
              }
            },
            "loc": {
              "start": {
                "line": 1,
                "column": 13
              },
              "end": {
                "line": 1,
                "column": 15
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 12
          },
          "end": {
            "line": 1,
            "column": 15
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 6
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 7
            },
            "end": {
              "line": 5,
              "column": 8
            }
          }
        },
        "typeParameters": {
          "type": "TSTypeParameterDeclaration",
          "params": [
            {
              "type": "TSTypeParameter",
              "name": {
                "type": "Identifier",
                "name": "U",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 9
                  },
                  "end": {
                    "line": 5,
                    "column": 10
                  }
                }
              },
              "constraint": {
                "type": "ETSTypeReference",
                "part": {
                  "type": "ETSTypeReferencePart",
                  "name": {
                    "type": "Identifier",
                    "name": "A",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 19
                      },
                      "end": {
                        "line": 5,
                        "column": 20
                      }
                    }
                  },
                  "typeParams": {
                    "type": "TSTypeParameterInstantiation",
                    "params": [
                      {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "Object",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 5,
                                "column": 21
                              },
                              "end": {
                                "line": 5,
                                "column": 27
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 5,
                              "column": 21
                            },
                            "end": {
                              "line": 5,
                              "column": 29
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 21
                          },
                          "end": {
                            "line": 5,
                            "column": 29
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 20
                      },
                      "end": {
                        "line": 5,
                        "column": 29
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 19
                    },
                    "end": {
                      "line": 5,
                      "column": 29
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 19
                  },
                  "end": {
                    "line": 5,
                    "column": 29
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 5,
                  "column": 9
                },
                "end": {
                  "line": 5,
                  "column": 29
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 5,
              "column": 8
            },
            "end": {
              "line": 5,
              "column": 29
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "my_var",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 13
                },
                "end": {
                  "line": 6,
                  "column": 19
                }
              }
            },
            "accessibility": "private",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "U",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 21
                    },
                    "end": {
                      "line": 6,
                      "column": 22
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 21
                  },
                  "end": {
                    "line": 6,
                    "column": 23
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 21
                },
                "end": {
                  "line": 6,
                  "column": 23
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 8,
                  "column": 5
                },
                "end": {
                  "line": 8,
                  "column": 8
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "bar",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 5
                    },
                    "end": {
                      "line": 8,
                      "column": 8
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 12
                    },
                    "end": {
                      "line": 8,
                      "column": 16
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "Object",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 9,
                                      "column": 16
                                    },
                                    "end": {
                                      "line": 9,
                                      "column": 22
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 9,
                                    "column": 16
                                  },
                                  "end": {
                                    "line": 9,
                                    "column": 24
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 9,
                                  "column": 16
                                },
                                "end": {
                                  "line": 9,
                                  "column": 24
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 9,
                                "column": 13
                              },
                              "end": {
                                "line": 9,
                                "column": 14
                              }
                            }
                          },
                          "init": {
                            "type": "MemberExpression",
                            "object": {
                              "type": "ThisExpression",
                              "loc": {
                                "start": {
                                  "line": 9,
                                  "column": 25
                                },
                                "end": {
                                  "line": 9,
                                  "column": 29
                                }
                              }
                            },
                            "property": {
                              "type": "Identifier",
                              "name": "my_var",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 9,
                                  "column": 30
                                },
                                "end": {
                                  "line": 9,
                                  "column": 36
                                }
                              }
                            },
                            "computed": false,
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 9,
                                "column": 25
                              },
                              "end": {
                                "line": 9,
                                "column": 36
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 9,
                              "column": 13
                            },
                            "end": {
                              "line": 9,
                              "column": 36
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 9,
                          "column": 9
                        },
                        "end": {
                          "line": 9,
                          "column": 37
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 17
                    },
                    "end": {
                      "line": 10,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 8,
                    "column": 8
                  },
                  "end": {
                    "line": 10,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 8,
                  "column": 8
                },
                "end": {
                  "line": 10,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 8,
                "column": 5
              },
              "end": {
                "line": 10,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 11,
                "column": 2
              },
              "end": {
                "line": 11,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 29
          },
          "end": {
            "line": 11,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 11,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 12,
      "column": 1
    }
  }
}
