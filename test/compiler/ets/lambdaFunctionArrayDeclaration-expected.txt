{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 18
                    },
                    "end": {
                      "line": 1,
                      "column": 21
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "src14",
                            "typeAnnotation": {
                              "type": "TSArrayType",
                              "elementType": {
                                "type": "ETSFunctionType",
                                "params": [
                                  {
                                    "type": "ETSParameterExpression",
                                    "name": {
                                      "type": "Identifier",
                                      "name": "x",
                                      "typeAnnotation": {
                                        "type": "ETSTypeReference",
                                        "part": {
                                          "type": "ETSTypeReferencePart",
                                          "name": {
                                            "type": "Identifier",
                                            "name": "Char",
                                            "decorators": [],
                                            "loc": {
                                              "start": {
                                                "line": 2,
                                                "column": 19
                                              },
                                              "end": {
                                                "line": 2,
                                                "column": 23
                                              }
                                            }
                                          },
                                          "loc": {
                                            "start": {
                                              "line": 2,
                                              "column": 19
                                            },
                                            "end": {
                                              "line": 2,
                                              "column": 24
                                            }
                                          }
                                        },
                                        "loc": {
                                          "start": {
                                            "line": 2,
                                            "column": 19
                                          },
                                          "end": {
                                            "line": 2,
                                            "column": 24
                                          }
                                        }
                                      },
                                      "decorators": [],
                                      "loc": {
                                        "start": {
                                          "line": 2,
                                          "column": 16
                                        },
                                        "end": {
                                          "line": 2,
                                          "column": 24
                                        }
                                      }
                                    },
                                    "loc": {
                                      "start": {
                                        "line": 2,
                                        "column": 16
                                      },
                                      "end": {
                                        "line": 2,
                                        "column": 24
                                      }
                                    }
                                  }
                                ],
                                "returnType": {
                                  "type": "ETSPrimitiveType",
                                  "loc": {
                                    "start": {
                                      "line": 2,
                                      "column": 28
                                    },
                                    "end": {
                                      "line": 2,
                                      "column": 32
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 2,
                                    "column": 14
                                  },
                                  "end": {
                                    "line": 2,
                                    "column": 32
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 2,
                                  "column": 36
                                },
                                "end": {
                                  "line": 2,
                                  "column": 37
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 7
                              },
                              "end": {
                                "line": 2,
                                "column": 12
                              }
                            }
                          },
                          "init": {
                            "type": "ArrayExpression",
                            "elements": [
                              {
                                "type": "ArrowFunctionExpression",
                                "function": {
                                  "type": "ScriptFunction",
                                  "id": null,
                                  "generator": false,
                                  "async": false,
                                  "expression": false,
                                  "params": [
                                    {
                                      "type": "ETSParameterExpression",
                                      "name": {
                                        "type": "Identifier",
                                        "name": "p",
                                        "typeAnnotation": {
                                          "type": "ETSTypeReference",
                                          "part": {
                                            "type": "ETSTypeReferencePart",
                                            "name": {
                                              "type": "Identifier",
                                              "name": "Char",
                                              "decorators": [],
                                              "loc": {
                                                "start": {
                                                  "line": 2,
                                                  "column": 43
                                                },
                                                "end": {
                                                  "line": 2,
                                                  "column": 47
                                                }
                                              }
                                            },
                                            "loc": {
                                              "start": {
                                                "line": 2,
                                                "column": 43
                                              },
                                              "end": {
                                                "line": 2,
                                                "column": 48
                                              }
                                            }
                                          },
                                          "loc": {
                                            "start": {
                                              "line": 2,
                                              "column": 43
                                            },
                                            "end": {
                                              "line": 2,
                                              "column": 48
                                            }
                                          }
                                        },
                                        "decorators": [],
                                        "loc": {
                                          "start": {
                                            "line": 2,
                                            "column": 40
                                          },
                                          "end": {
                                            "line": 2,
                                            "column": 48
                                          }
                                        }
                                      },
                                      "loc": {
                                        "start": {
                                          "line": 2,
                                          "column": 40
                                        },
                                        "end": {
                                          "line": 2,
                                          "column": 48
                                        }
                                      }
                                    }
                                  ],
                                  "returnType": {
                                    "type": "ETSPrimitiveType",
                                    "loc": {
                                      "start": {
                                        "line": 2,
                                        "column": 50
                                      },
                                      "end": {
                                        "line": 2,
                                        "column": 54
                                      }
                                    }
                                  },
                                  "body": {
                                    "type": "BlockStatement",
                                    "statements": [
                                      {
                                        "type": "ReturnStatement",
                                        "argument": {
                                          "type": "CallExpression",
                                          "callee": {
                                            "type": "MemberExpression",
                                            "object": {
                                              "type": "Identifier",
                                              "name": "p",
                                              "decorators": [],
                                              "loc": {
                                                "start": {
                                                  "line": 2,
                                                  "column": 67
                                                },
                                                "end": {
                                                  "line": 2,
                                                  "column": 68
                                                }
                                              }
                                            },
                                            "property": {
                                              "type": "Identifier",
                                              "name": "unboxed",
                                              "decorators": [],
                                              "loc": {
                                                "start": {
                                                  "line": 2,
                                                  "column": 69
                                                },
                                                "end": {
                                                  "line": 2,
                                                  "column": 76
                                                }
                                              }
                                            },
                                            "computed": false,
                                            "optional": false,
                                            "loc": {
                                              "start": {
                                                "line": 2,
                                                "column": 67
                                              },
                                              "end": {
                                                "line": 2,
                                                "column": 76
                                              }
                                            }
                                          },
                                          "arguments": [],
                                          "optional": false,
                                          "loc": {
                                            "start": {
                                              "line": 2,
                                              "column": 67
                                            },
                                            "end": {
                                              "line": 2,
                                              "column": 78
                                            }
                                          }
                                        },
                                        "loc": {
                                          "start": {
                                            "line": 2,
                                            "column": 60
                                          },
                                          "end": {
                                            "line": 2,
                                            "column": 79
                                          }
                                        }
                                      }
                                    ],
                                    "loc": {
                                      "start": {
                                        "line": 2,
                                        "column": 58
                                      },
                                      "end": {
                                        "line": 2,
                                        "column": 81
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 2,
                                      "column": 39
                                    },
                                    "end": {
                                      "line": 2,
                                      "column": 81
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 2,
                                    "column": 39
                                  },
                                  "end": {
                                    "line": 2,
                                    "column": 81
                                  }
                                }
                              }
                            ],
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 38
                              },
                              "end": {
                                "line": 2,
                                "column": 82
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 7
                            },
                            "end": {
                              "line": 2,
                              "column": 82
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 3
                        },
                        "end": {
                          "line": 2,
                          "column": 83
                        }
                      }
                    },
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "NumberLiteral",
                        "value": 0,
                        "loc": {
                          "start": {
                            "line": 3,
                            "column": 10
                          },
                          "end": {
                            "line": 3,
                            "column": 11
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 3
                        },
                        "end": {
                          "line": 3,
                          "column": 12
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 22
                    },
                    "end": {
                      "line": 4,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 14
                  },
                  "end": {
                    "line": 4,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 14
                },
                "end": {
                  "line": 4,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 4,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 1
    }
  }
}
