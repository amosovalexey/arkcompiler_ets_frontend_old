{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 18
                    },
                    "end": {
                      "line": 1,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "mystring",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "String",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 2,
                                      "column": 18
                                    },
                                    "end": {
                                      "line": 2,
                                      "column": 24
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 2,
                                    "column": 18
                                  },
                                  "end": {
                                    "line": 2,
                                    "column": 26
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 2,
                                  "column": 18
                                },
                                "end": {
                                  "line": 2,
                                  "column": 26
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 7
                              },
                              "end": {
                                "line": 2,
                                "column": 15
                              }
                            }
                          },
                          "init": {
                            "type": "StringLiteral",
                            "value": "",
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 27
                              },
                              "end": {
                                "line": 2,
                                "column": 29
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 7
                            },
                            "end": {
                              "line": 2,
                              "column": 29
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 3
                        },
                        "end": {
                          "line": 2,
                          "column": 30
                        }
                      }
                    },
                    {
                      "type": "ForUpdateStatement",
                      "init": {
                        "type": "VariableDeclaration",
                        "declarations": [
                          {
                            "type": "VariableDeclarator",
                            "id": {
                              "type": "Identifier",
                              "name": "a",
                              "typeAnnotation": {
                                "type": "ETSPrimitiveType",
                                "loc": {
                                  "start": {
                                    "line": 4,
                                    "column": 15
                                  },
                                  "end": {
                                    "line": 4,
                                    "column": 19
                                  }
                                }
                              },
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 12
                                },
                                "end": {
                                  "line": 4,
                                  "column": 13
                                }
                              }
                            },
                            "init": {
                              "type": "CharLiteral",
                              "value": "a",
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 22
                                },
                                "end": {
                                  "line": 4,
                                  "column": 26
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 12
                              },
                              "end": {
                                "line": 4,
                                "column": 26
                              }
                            }
                          }
                        ],
                        "kind": "let",
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 8
                          },
                          "end": {
                            "line": 4,
                            "column": 26
                          }
                        }
                      },
                      "test": {
                        "type": "BinaryExpression",
                        "operator": "<=",
                        "left": {
                          "type": "Identifier",
                          "name": "a",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 28
                            },
                            "end": {
                              "line": 4,
                              "column": 29
                            }
                          }
                        },
                        "right": {
                          "type": "CharLiteral",
                          "value": "d",
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 33
                            },
                            "end": {
                              "line": 4,
                              "column": 37
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 28
                          },
                          "end": {
                            "line": 4,
                            "column": 37
                          }
                        }
                      },
                      "update": {
                        "type": "UpdateExpression",
                        "operator": "++",
                        "prefix": false,
                        "argument": {
                          "type": "Identifier",
                          "name": "a",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 39
                            },
                            "end": {
                              "line": 4,
                              "column": 40
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 39
                          },
                          "end": {
                            "line": 4,
                            "column": 42
                          }
                        }
                      },
                      "body": {
                        "type": "BlockStatement",
                        "statements": [
                          {
                            "type": "ExpressionStatement",
                            "expression": {
                              "type": "AssignmentExpression",
                              "operator": "+=",
                              "left": {
                                "type": "Identifier",
                                "name": "mystring",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 5,
                                    "column": 5
                                  },
                                  "end": {
                                    "line": 5,
                                    "column": 13
                                  }
                                }
                              },
                              "right": {
                                "type": "Identifier",
                                "name": "a",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 5,
                                    "column": 17
                                  },
                                  "end": {
                                    "line": 5,
                                    "column": 18
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 5,
                                  "column": 5
                                },
                                "end": {
                                  "line": 5,
                                  "column": 18
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 5,
                                "column": 5
                              },
                              "end": {
                                "line": 5,
                                "column": 19
                              }
                            }
                          }
                        ],
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 44
                          },
                          "end": {
                            "line": 6,
                            "column": 4
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 4,
                          "column": 3
                        },
                        "end": {
                          "line": 6,
                          "column": 4
                        }
                      }
                    },
                    {
                      "type": "AssertStatement",
                      "test": {
                        "type": "BinaryExpression",
                        "operator": "==",
                        "left": {
                          "type": "Identifier",
                          "name": "mystring",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 8,
                              "column": 10
                            },
                            "end": {
                              "line": 8,
                              "column": 18
                            }
                          }
                        },
                        "right": {
                          "type": "StringLiteral",
                          "value": "abcd",
                          "loc": {
                            "start": {
                              "line": 8,
                              "column": 22
                            },
                            "end": {
                              "line": 8,
                              "column": 28
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 8,
                            "column": 9
                          },
                          "end": {
                            "line": 8,
                            "column": 29
                          }
                        }
                      },
                      "second": null,
                      "loc": {
                        "start": {
                          "line": 8,
                          "column": 3
                        },
                        "end": {
                          "line": 8,
                          "column": 30
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 23
                    },
                    "end": {
                      "line": 9,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 14
                  },
                  "end": {
                    "line": 9,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 14
                },
                "end": {
                  "line": 9,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 9,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 10,
      "column": 1
    }
  }
}
