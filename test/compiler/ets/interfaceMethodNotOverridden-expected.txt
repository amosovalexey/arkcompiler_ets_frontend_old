{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 8
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 5
                    },
                    "end": {
                      "line": 2,
                      "column": 8
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 12
                    },
                    "end": {
                      "line": 2,
                      "column": 16
                    }
                  }
                },
                "declare": true,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 8
                  },
                  "end": {
                    "line": 2,
                    "column": 16
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 16
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 16
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "A",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 6
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 7
            },
            "end": {
              "line": 5,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [
          {
            "type": "TSClassImplements",
            "expression": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "A",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 20
                    },
                    "end": {
                      "line": 5,
                      "column": 21
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 20
                  },
                  "end": {
                    "line": 5,
                    "column": 23
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 5,
                  "column": 20
                },
                "end": {
                  "line": 5,
                  "column": 23
                }
              }
            },
            "loc": {
              "start": {
                "line": 5,
                "column": 20
              },
              "end": {
                "line": 5,
                "column": 23
              }
            }
          }
        ],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 12
                },
                "end": {
                  "line": 6,
                  "column": 15
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 12
                    },
                    "end": {
                      "line": 6,
                      "column": 15
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 6,
                            "column": 19
                          },
                          "end": {
                            "line": 6,
                            "column": 22
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 6,
                          "column": 16
                        },
                        "end": {
                          "line": 6,
                          "column": 22
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 16
                      },
                      "end": {
                        "line": 6,
                        "column": 22
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 25
                    },
                    "end": {
                      "line": 6,
                      "column": 29
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 30
                    },
                    "end": {
                      "line": 8,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 15
                  },
                  "end": {
                    "line": 8,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 15
                },
                "end": {
                  "line": 8,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 8,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 9,
                "column": 2
              },
              "end": {
                "line": 9,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 22
          },
          "end": {
            "line": 9,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 9,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 10,
      "column": 1
    }
  }
}
TypeError: B is not abstract and does not override abstract method foo(): void in A [interfaceMethodNotOverridden.ets:5:22]
