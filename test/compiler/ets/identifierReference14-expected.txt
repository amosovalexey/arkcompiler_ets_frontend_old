{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 6,
              "column": 7
            },
            "end": {
              "line": 6,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 3
                },
                "end": {
                  "line": 7,
                  "column": 6
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 3
                    },
                    "end": {
                      "line": 7,
                      "column": 6
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 10
                    },
                    "end": {
                      "line": 7,
                      "column": 14
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 15
                    },
                    "end": {
                      "line": 9,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 6
                  },
                  "end": {
                    "line": 9,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 7,
                  "column": 6
                },
                "end": {
                  "line": 9,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 7,
                "column": 3
              },
              "end": {
                "line": 9,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 10,
                "column": 2
              },
              "end": {
                "line": 10,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 6,
            "column": 9
          },
          "end": {
            "line": 10,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 6,
          "column": 1
        },
        "end": {
          "line": 10,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 12,
              "column": 7
            },
            "end": {
              "line": 12,
              "column": 8
            }
          }
        },
        "superClass": {
          "type": "ETSTypeReference",
          "part": {
            "type": "ETSTypeReferencePart",
            "name": {
              "type": "Identifier",
              "name": "B",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 12,
                  "column": 17
                },
                "end": {
                  "line": 12,
                  "column": 18
                }
              }
            },
            "loc": {
              "start": {
                "line": 12,
                "column": 17
              },
              "end": {
                "line": 12,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 12,
              "column": 17
            },
            "end": {
              "line": 12,
              "column": 20
            }
          }
        },
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 13,
                  "column": 3
                },
                "end": {
                  "line": 13,
                  "column": 6
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 13,
                      "column": 3
                    },
                    "end": {
                      "line": 13,
                      "column": 6
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 13,
                            "column": 10
                          },
                          "end": {
                            "line": 13,
                            "column": 13
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 13,
                          "column": 7
                        },
                        "end": {
                          "line": 13,
                          "column": 13
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 13,
                        "column": 7
                      },
                      "end": {
                        "line": 13,
                        "column": 13
                      }
                    }
                  },
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "b",
                      "typeAnnotation": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 13,
                            "column": 18
                          },
                          "end": {
                            "line": 13,
                            "column": 21
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 13,
                          "column": 15
                        },
                        "end": {
                          "line": 13,
                          "column": 21
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 13,
                        "column": 15
                      },
                      "end": {
                        "line": 13,
                        "column": 21
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 13,
                      "column": 24
                    },
                    "end": {
                      "line": 13,
                      "column": 28
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 13,
                      "column": 29
                    },
                    "end": {
                      "line": 15,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 13,
                    "column": 6
                  },
                  "end": {
                    "line": 15,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 13,
                  "column": 6
                },
                "end": {
                  "line": 15,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 13,
                "column": 3
              },
              "end": {
                "line": 15,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 3
                },
                "end": {
                  "line": 17,
                  "column": 6
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "bar",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 3
                    },
                    "end": {
                      "line": 17,
                      "column": 6
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 10
                    },
                    "end": {
                      "line": 17,
                      "column": 14
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "CallExpression",
                        "callee": {
                          "type": "Identifier",
                          "name": "foo",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 5
                            },
                            "end": {
                              "line": 18,
                              "column": 8
                            }
                          }
                        },
                        "arguments": [
                          {
                            "type": "NumberLiteral",
                            "value": 1,
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 9
                              },
                              "end": {
                                "line": 18,
                                "column": 10
                              }
                            }
                          }
                        ],
                        "optional": false,
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 5
                          },
                          "end": {
                            "line": 18,
                            "column": 11
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 5
                        },
                        "end": {
                          "line": 18,
                          "column": 12
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 15
                    },
                    "end": {
                      "line": 19,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 6
                  },
                  "end": {
                    "line": 19,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 6
                },
                "end": {
                  "line": 19,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 3
              },
              "end": {
                "line": 19,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 20,
                "column": 2
              },
              "end": {
                "line": 20,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 12,
            "column": 19
          },
          "end": {
            "line": 20,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 12,
          "column": 1
        },
        "end": {
          "line": 20,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 10
                },
                "end": {
                  "line": 2,
                  "column": 13
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 10
                    },
                    "end": {
                      "line": 2,
                      "column": 13
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 17
                          },
                          "end": {
                            "line": 2,
                            "column": 20
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 14
                        },
                        "end": {
                          "line": 2,
                          "column": 20
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 14
                      },
                      "end": {
                        "line": 2,
                        "column": 20
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 23
                    },
                    "end": {
                      "line": 2,
                      "column": 27
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 28
                    },
                    "end": {
                      "line": 4,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 13
                  },
                  "end": {
                    "line": 4,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 13
                },
                "end": {
                  "line": 4,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 1
              },
              "end": {
                "line": 4,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 21,
      "column": 1
    }
  }
}
