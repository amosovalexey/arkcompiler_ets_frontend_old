{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "reject",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 16
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "reject",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 16
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "error",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "Object",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 24
                              },
                              "end": {
                                "line": 1,
                                "column": 30
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 24
                            },
                            "end": {
                              "line": 1,
                              "column": 31
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 24
                          },
                          "end": {
                            "line": 1,
                            "column": 31
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 17
                        },
                        "end": {
                          "line": 1,
                          "column": 31
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 17
                      },
                      "end": {
                        "line": 1,
                        "column": 31
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 33
                    },
                    "end": {
                      "line": 1,
                      "column": 37
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 38
                    },
                    "end": {
                      "line": 2,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 16
                  },
                  "end": {
                    "line": 2,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 16
                },
                "end": {
                  "line": 2,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 2,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 10
                },
                "end": {
                  "line": 4,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 10
                    },
                    "end": {
                      "line": 4,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 18
                    },
                    "end": {
                      "line": 4,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "fn",
                            "typeAnnotation": {
                              "type": "ETSFunctionType",
                              "params": [],
                              "returnType": {
                                "type": "ETSPrimitiveType",
                                "loc": {
                                  "start": {
                                    "line": 5,
                                    "column": 19
                                  },
                                  "end": {
                                    "line": 5,
                                    "column": 23
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 5,
                                  "column": 13
                                },
                                "end": {
                                  "line": 5,
                                  "column": 23
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 5,
                                "column": 9
                              },
                              "end": {
                                "line": 5,
                                "column": 11
                              }
                            }
                          },
                          "init": {
                            "type": "ArrowFunctionExpression",
                            "function": {
                              "type": "ScriptFunction",
                              "id": null,
                              "generator": false,
                              "async": false,
                              "expression": false,
                              "params": [],
                              "returnType": {
                                "type": "ETSPrimitiveType",
                                "loc": {
                                  "start": {
                                    "line": 5,
                                    "column": 30
                                  },
                                  "end": {
                                    "line": 5,
                                    "column": 34
                                  }
                                }
                              },
                              "body": {
                                "type": "BlockStatement",
                                "statements": [
                                  {
                                    "type": "TryStatement",
                                    "block": {
                                      "type": "BlockStatement",
                                      "statements": [],
                                      "loc": {
                                        "start": {
                                          "line": 6,
                                          "column": 13
                                        },
                                        "end": {
                                          "line": 7,
                                          "column": 10
                                        }
                                      }
                                    },
                                    "handler": [
                                      {
                                        "type": "CatchClause",
                                        "body": {
                                          "type": "BlockStatement",
                                          "statements": [
                                            {
                                              "type": "ExpressionStatement",
                                              "expression": {
                                                "type": "CallExpression",
                                                "callee": {
                                                  "type": "Identifier",
                                                  "name": "reject",
                                                  "decorators": [],
                                                  "loc": {
                                                    "start": {
                                                      "line": 8,
                                                      "column": 13
                                                    },
                                                    "end": {
                                                      "line": 8,
                                                      "column": 19
                                                    }
                                                  }
                                                },
                                                "arguments": [
                                                  {
                                                    "type": "Identifier",
                                                    "name": "error",
                                                    "decorators": [],
                                                    "loc": {
                                                      "start": {
                                                        "line": 8,
                                                        "column": 20
                                                      },
                                                      "end": {
                                                        "line": 8,
                                                        "column": 25
                                                      }
                                                    }
                                                  }
                                                ],
                                                "optional": false,
                                                "loc": {
                                                  "start": {
                                                    "line": 8,
                                                    "column": 13
                                                  },
                                                  "end": {
                                                    "line": 8,
                                                    "column": 26
                                                  }
                                                }
                                              },
                                              "loc": {
                                                "start": {
                                                  "line": 8,
                                                  "column": 13
                                                },
                                                "end": {
                                                  "line": 8,
                                                  "column": 27
                                                }
                                              }
                                            }
                                          ],
                                          "loc": {
                                            "start": {
                                              "line": 7,
                                              "column": 36
                                            },
                                            "end": {
                                              "line": 9,
                                              "column": 10
                                            }
                                          }
                                        },
                                        "param": {
                                          "type": "Identifier",
                                          "name": "error",
                                          "typeAnnotation": {
                                            "type": "ETSTypeReference",
                                            "part": {
                                              "type": "ETSTypeReferencePart",
                                              "name": {
                                                "type": "Identifier",
                                                "name": "Exception",
                                                "decorators": [],
                                                "loc": {
                                                  "start": {
                                                    "line": 7,
                                                    "column": 25
                                                  },
                                                  "end": {
                                                    "line": 7,
                                                    "column": 34
                                                  }
                                                }
                                              },
                                              "loc": {
                                                "start": {
                                                  "line": 7,
                                                  "column": 25
                                                },
                                                "end": {
                                                  "line": 7,
                                                  "column": 35
                                                }
                                              }
                                            },
                                            "loc": {
                                              "start": {
                                                "line": 7,
                                                "column": 25
                                              },
                                              "end": {
                                                "line": 7,
                                                "column": 35
                                              }
                                            }
                                          },
                                          "decorators": [],
                                          "loc": {
                                            "start": {
                                              "line": 7,
                                              "column": 18
                                            },
                                            "end": {
                                              "line": 7,
                                              "column": 23
                                            }
                                          }
                                        },
                                        "loc": {
                                          "start": {
                                            "line": 7,
                                            "column": 11
                                          },
                                          "end": {
                                            "line": 9,
                                            "column": 10
                                          }
                                        }
                                      }
                                    ],
                                    "finalizer": null,
                                    "loc": {
                                      "start": {
                                        "line": 6,
                                        "column": 9
                                      },
                                      "end": {
                                        "line": 9,
                                        "column": 10
                                      }
                                    }
                                  }
                                ],
                                "loc": {
                                  "start": {
                                    "line": 5,
                                    "column": 38
                                  },
                                  "end": {
                                    "line": 10,
                                    "column": 6
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 5,
                                  "column": 26
                                },
                                "end": {
                                  "line": 10,
                                  "column": 6
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 5,
                                "column": 26
                              },
                              "end": {
                                "line": 10,
                                "column": 6
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 5,
                              "column": 9
                            },
                            "end": {
                              "line": 10,
                              "column": 6
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 5
                        },
                        "end": {
                          "line": 10,
                          "column": 6
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 23
                    },
                    "end": {
                      "line": 11,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 14
                  },
                  "end": {
                    "line": 11,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 14
                },
                "end": {
                  "line": 11,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 1
              },
              "end": {
                "line": 11,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 12,
      "column": 1
    }
  }
}
