{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 7
            },
            "end": {
              "line": 1,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "x",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 12
                },
                "end": {
                  "line": 2,
                  "column": 13
                }
              }
            },
            "accessibility": "internal",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 16
                },
                "end": {
                  "line": 2,
                  "column": 19
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "y",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 12
                },
                "end": {
                  "line": 3,
                  "column": 13
                }
              }
            },
            "accessibility": "internal",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 16
                },
                "end": {
                  "line": 3,
                  "column": 19
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "accessibility": "internal",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "x",
                      "typeAnnotation": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 28
                          },
                          "end": {
                            "line": 5,
                            "column": 31
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 24
                        },
                        "end": {
                          "line": 5,
                          "column": 31
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 24
                      },
                      "end": {
                        "line": 5,
                        "column": 31
                      }
                    }
                  },
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "y",
                      "typeAnnotation": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 37
                          },
                          "end": {
                            "line": 5,
                            "column": 40
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 33
                        },
                        "end": {
                          "line": 5,
                          "column": 40
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 33
                      },
                      "end": {
                        "line": 5,
                        "column": 40
                      }
                    }
                  }
                ],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "ThisExpression",
                            "loc": {
                              "start": {
                                "line": 6,
                                "column": 5
                              },
                              "end": {
                                "line": 6,
                                "column": 9
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "x",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 6,
                                "column": 10
                              },
                              "end": {
                                "line": 6,
                                "column": 11
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 6,
                              "column": 5
                            },
                            "end": {
                              "line": 6,
                              "column": 11
                            }
                          }
                        },
                        "right": {
                          "type": "Identifier",
                          "name": "x",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 6,
                              "column": 14
                            },
                            "end": {
                              "line": 6,
                              "column": 15
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 6,
                            "column": 5
                          },
                          "end": {
                            "line": 6,
                            "column": 15
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 6,
                          "column": 5
                        },
                        "end": {
                          "line": 6,
                          "column": 16
                        }
                      }
                    },
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "ThisExpression",
                            "loc": {
                              "start": {
                                "line": 7,
                                "column": 5
                              },
                              "end": {
                                "line": 7,
                                "column": 9
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "y",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 7,
                                "column": 10
                              },
                              "end": {
                                "line": 7,
                                "column": 11
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 7,
                              "column": 5
                            },
                            "end": {
                              "line": 7,
                              "column": 11
                            }
                          }
                        },
                        "right": {
                          "type": "Identifier",
                          "name": "y",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 7,
                              "column": 14
                            },
                            "end": {
                              "line": 7,
                              "column": 15
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 7,
                            "column": 5
                          },
                          "end": {
                            "line": 7,
                            "column": 15
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 7,
                          "column": 5
                        },
                        "end": {
                          "line": 7,
                          "column": 16
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 42
                    },
                    "end": {
                      "line": 8,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 23
                  },
                  "end": {
                    "line": 8,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 5,
                  "column": 23
                },
                "end": {
                  "line": 8,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 3
              },
              "end": {
                "line": 8,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 10,
                  "column": 12
                },
                "end": {
                  "line": 10,
                  "column": 15
                }
              }
            },
            "kind": "method",
            "accessibility": "internal",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 10,
                      "column": 12
                    },
                    "end": {
                      "line": 10,
                      "column": 15
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 10,
                      "column": 19
                    },
                    "end": {
                      "line": 10,
                      "column": 23
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 10,
                      "column": 24
                    },
                    "end": {
                      "line": 10,
                      "column": 26
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 10,
                    "column": 15
                  },
                  "end": {
                    "line": 10,
                    "column": 26
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 10,
                  "column": 15
                },
                "end": {
                  "line": 10,
                  "column": 26
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 10,
                "column": 3
              },
              "end": {
                "line": 10,
                "column": 26
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 9
          },
          "end": {
            "line": 11,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 11,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 13,
                  "column": 10
                },
                "end": {
                  "line": 13,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 13,
                      "column": 10
                    },
                    "end": {
                      "line": 13,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 13,
                      "column": 18
                    },
                    "end": {
                      "line": 13,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "instance",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "A",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 14,
                                      "column": 18
                                    },
                                    "end": {
                                      "line": 14,
                                      "column": 19
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 14,
                                    "column": 18
                                  },
                                  "end": {
                                    "line": 14,
                                    "column": 21
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 14,
                                  "column": 18
                                },
                                "end": {
                                  "line": 14,
                                  "column": 21
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 14,
                                "column": 7
                              },
                              "end": {
                                "line": 14,
                                "column": 15
                              }
                            }
                          },
                          "init": {
                            "type": "ETSNewClassInstanceExpression",
                            "typeReference": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "A",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 14,
                                      "column": 26
                                    },
                                    "end": {
                                      "line": 14,
                                      "column": 27
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 14,
                                    "column": 26
                                  },
                                  "end": {
                                    "line": 14,
                                    "column": 28
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 14,
                                  "column": 26
                                },
                                "end": {
                                  "line": 14,
                                  "column": 28
                                }
                              }
                            },
                            "arguments": [
                              {
                                "type": "NumberLiteral",
                                "value": 2,
                                "loc": {
                                  "start": {
                                    "line": 14,
                                    "column": 28
                                  },
                                  "end": {
                                    "line": 14,
                                    "column": 29
                                  }
                                }
                              },
                              {
                                "type": "NumberLiteral",
                                "value": 3,
                                "loc": {
                                  "start": {
                                    "line": 14,
                                    "column": 31
                                  },
                                  "end": {
                                    "line": 14,
                                    "column": 32
                                  }
                                }
                              }
                            ],
                            "loc": {
                              "start": {
                                "line": 14,
                                "column": 22
                              },
                              "end": {
                                "line": 14,
                                "column": 34
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 14,
                              "column": 7
                            },
                            "end": {
                              "line": 14,
                              "column": 34
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 14,
                          "column": 3
                        },
                        "end": {
                          "line": 14,
                          "column": 34
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 13,
                      "column": 23
                    },
                    "end": {
                      "line": 15,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 13,
                    "column": 14
                  },
                  "end": {
                    "line": 15,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 13,
                  "column": 14
                },
                "end": {
                  "line": 15,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 13,
                "column": 1
              },
              "end": {
                "line": 15,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 16,
      "column": 1
    }
  }
}
